package z_old;

/*
public class PrvniProjekt extends GraphicWindow {
    private int mode = 0;

    private Point start;
    private Point end;
    private List<Point> points = new ArrayList<>();
    private int peakCount = 3;
    private int clickCount = 0;

    public static void main(String[] args) {
        SwingUtilities.invokeLater(PrvniProjekt::new);
    }


    @Override
    public void setup() {
        window.setTitle("Projekt 01 - David Tláskal");
    }

    @Override
    public void draw() {
        canvas.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_C:
                        renderer.clear();
                        if (mode == 1) {
                            points.clear();
                        }
                        break;
                    case KeyEvent.VK_M:
                        renderer.clear();

                        start = null;
                        end = null;
                        points.clear();
                        peakCount = 3;
                        clickCount = 0;

                        if (mode < 2) {
                            mode++;
                        } else {
                            mode = 0;
                        }
                        break;
                    case KeyEvent.VK_UP:
                        peakCount++;
                        break;
                    case KeyEvent.VK_DOWN:
                        if (peakCount > 3) {
                            peakCount--;
                        }
                        break;
                }
            }
        });

        canvas.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (mode == 0) {
                    start = new Point(e.getX(), e.getY());
                } else if (mode == 1) {
                    renderer.clear();
                    points.add(new Point(e.getX(), e.getY()));
                    if (points.size() >= 3) {
                        renderer.drawPolygon(points, Color.BLUE.getRGB());
                    }
                    for (Point point : points) {
                        renderer.drawPixel(
                                (int) point.getX(),
                                (int) point.getY(),
                                Color.PINK.getRGB()
                        );
                    }
                } else if (mode == 2) {
                    if (clickCount < 3) {
                        clickCount++;
                    } else {
                        clickCount = 0;
                        renderer.clear();
                    }

                    switch (clickCount) {
                        case 1:
                            start = new Point(
                                    e.getX(),
                                    e.getY()
                            );
                            break;
                        case 2:
                            end = new Point(
                                    e.getX(),
                                    e.getY()
                            );
                            break;
                        case 3:
                            renderer.clear();
                            renderer.drawRegularPolygon(
                                    (int) start.getX(),
                                    (int) start.getY(),
                                    e.getX(),
                                    e.getY(),
                                    peakCount,
                                    Color.BLUE.getRGB()
                            );
                            break;
                    }
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (mode == 0) {
                    end = new Point(e.getX(), e.getY());
                    renderer.drawLine(
                            (int) start.getX(),
                            (int) start.getY(),
                            (int) end.getX(),
                            (int) end.getY(),
                            Color.YELLOW.getRGB()
                    );
                }
            }
        });

        canvas.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (mode == 0) {
                    renderer.clear();
                    renderer.drawLine(
                            (int) start.getX(),
                            (int) start.getY(),
                            e.getX(),
                            e.getY(),
                            Color.RED.getRGB()
                    );
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                if (mode == 2 && clickCount == 1) {
                    renderer.clear();
                    renderer.drawRegularPolygon(
                            (int) start.getX(),
                            (int) start.getY(),
                            e.getX(),
                            e.getY(),
                            peakCount,
                            Color.RED.getRGB()
                    );
                } else if (mode == 2 && clickCount == 2) {
                    renderer.clear();
                    renderer.drawRegularPolygon(
                            (int) start.getX(),
                            (int) start.getY(),
                            e.getX(),
                            e.getY(),
                            peakCount,
                            Color.RED.getRGB()
                    );
                }
            }
        });
    }
}
*/