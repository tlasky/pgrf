package controller;

import models.Point;
import utils.Raster;
import utils.Renderer;
import utils.fill.ScanLineFiller;
import utils.fill.SeedFiller;
import view.GraphicWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.util.*;
import java.util.List;

public class GraphicController {
    private Raster raster;
    private Renderer renderer;
    private GraphicWindow window;

    private boolean first = true;
    private boolean sl = true;
    private List<Point> firstPolygon = new ArrayList<>();
    private List<Point> secondPolygon = new ArrayList<>();


    public GraphicController(GraphicWindow window) {
        this.window = window;

        initObjects();
        initListeners();
    }

    private void initObjects() {
        raster = window.getRaster();
        renderer = new Renderer(raster);
    }

    public void initListeners() {
        raster.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_S:
                        first = !first;
                        break;
                    case KeyEvent.VK_F:
                        sl = !sl;
                        break;
                    case KeyEvent.VK_C:
                        firstPolygon.clear();
                        secondPolygon.clear();
                        first = true;
                        raster.clear();
                        break;
                }
            }
        });

        raster.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    if (first) {
                        firstPolygon.add(new Point(e));
                    } else {
                        secondPolygon.add(new Point(e));
                    }
                    update();
                } else {
                    if (sl) {
                        new SeedFiller(raster, e.getX(), e.getY(), Color.PINK.getRGB()).fill();
                    } else {
                        ScanLineFiller sl = new ScanLineFiller();
                        sl.init(firstPolygon, Color.PINK.getRGB(), Color.GREEN.getRGB());
                        sl.setRaster(raster);
                        sl.setRenderer(renderer);
                        sl.fill();
                    }
                }
            }
        });

        raster.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    if (first) {
                        firstPolygon.get(firstPolygon.size() - 1).setX(e.getX()).setY(e.getY());
                    } else {
                        secondPolygon.get(secondPolygon.size() - 1).setX(e.getX()).setY(e.getY());
                    }
                    update();
                }
            }
        });
    }

    private void update() {
        raster.clear();
        renderer.drawPolygon(firstPolygon, Color.GREEN.getRGB());
        renderer.drawPolygon(secondPolygon, Color.BLUE.getRGB());
        if (firstPolygon.size() >= 3 && secondPolygon.size() >= 3) {
            renderer.drawPolygon(
                    renderer.clip(firstPolygon, secondPolygon),
                    Color.YELLOW.getRGB()
            );
        }

    }
}
