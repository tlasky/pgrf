package utils.fill;

import utils.Raster;

import java.awt.image.BufferedImage;

public interface Filler {
    void setRaster(Raster raster);
    void fill();
}

