package utils.fill;

import utils.Raster;

import java.awt.image.BufferedImage;

public class SeedFiller implements Filler {
    private Raster raster;

    private int seedX;
    private int seedY;
    private int fillColor;
    private int backColor;

    public SeedFiller(Raster raster, int seedX, int seedY, int fillColor) {
        this.seedX = seedX;
        this.seedY = seedY;
        this.fillColor = fillColor;
        setRaster(raster);
        backColor = raster.getImg().getRGB(seedX, seedY);
    }

    public void seedFill(int x, int y) {
        if (x >= 0 && y >= 0 && x < raster.getImg().getWidth() && y < raster.getImg().getHeight()) {
            if (backColor == raster.getImg().getRGB(x, y)) {
                raster.getImg().setRGB(x, y, fillColor);
                seedFill(x, y + 1);
                seedFill(x, y - 1);
                seedFill(x + 1, y);
                seedFill(x - 1, y);
            }
        }
    }

    @Override
    public void fill() {
        seedFill(seedX, seedY);
    }

    @Override
    public void setRaster(Raster raster) {
        this.raster = raster;
    }
}
