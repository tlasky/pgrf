package utils.fill;

import models.Edge;
import models.Point;
import utils.Raster;
import utils.Renderer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ScanLineFiller implements Filler {
    private Renderer renderer;
    private Raster raster;
    private int fillColor, edgeColor;
    private List<Point> points;

    @Override
    public void setRaster(Raster raster) {
        this.raster = raster;
    }

    public void setRenderer(Renderer renderer) {
        this.renderer = renderer;
    }

    @Override
    public void fill() {
        scanLine();
        renderer.drawPolygon(points, edgeColor);
    }

    public void init(List<Point> points, int fillColor, int edgeColor) {
        this.points = points;
        this.fillColor = fillColor;
        this.edgeColor = edgeColor;
    }

    private int ymax() {
        int max = points.get(0).getY();
        for (Point point : points) {
            if (point.getY() > max) {
                max = point.getY();
            }
        }
        return max;
    }

    private int ymin() {
        int min = points.get(0).getY();
        for (Point point : points) {
            if (point.getY() < min) {
                min = point.getY();
            }
        }
        return min;
    }

    private void scanLine() {
        List<Edge> edges = new ArrayList<>();

        for (int i = 0; i < points.size() - 1; i++) {
            Edge e = new Edge(
                    points.get(i),
                    points.get(i + 1)
            );

            if (!e.isHorizontal()) {
                e.orientate();
                edges.add(e);
            }
        }

        Edge lastEdge = new Edge(
                points.get(points.size() - 1),
                points.get(0)
        );
        if (!lastEdge.isHorizontal()) {
            edges.add(lastEdge);
        }

        for (int y = ymin(); y <= ymax(); y++) {
            List<Integer> intersections = new ArrayList<>();
            for (Edge edge : edges) {
                if (edge.intersectionExists(y)) {
                    intersections.add(edge.getIntersection(y));
                }
            }

            Collections.sort(intersections);

            for (int i = 0; i < intersections.size() - 1; i += 2) {
                renderer.drawLine(
                        intersections.get(i),
                        y,
                        intersections.get(i + 1),
                        y,
                        fillColor
                );
            }
        }
    }
}
