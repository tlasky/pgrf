package utils;

import models.Edge;
import models.Point;

import java.util.ArrayList;
import java.util.List;

public class Renderer {
    private Raster raster;

    public Renderer(Raster raster) {
        this.raster = raster;
    }

    public void drawLineTrivial(int x1, int y1, int x2, int y2, int color) {
        int dx = x1 - x2;
        int dy = y1 - y2;

        if (Math.abs(dx) > Math.abs(dy)) {
            if (x2 < x1) {
                int temp = x1;
                x1 = x2;
                x2 = temp;
                temp = y1;
                y1 = y2;
            }
            float k = (float) dy / (float) dx;
            for (int x = x1; x < x2; x++) {
                int y = y1 + (int) (k * (x - x1));
                raster.drawPixel(x, y, color);
            }
        } else {
            if (y2 < y1) {
                int temp = y1;
                y1 = y2;
                y2 = temp;
                temp = x1;
                x1 = x2;
            }
            float k = (float) dx / (float) dy;
            for (int y = y1; y < y2; y++) {
                int x = x1 + (int) (k * (y - y1));
                raster.drawPixel(x, y, color);
            }
        }
    }

    public void drawLineDDA(int x1, int y1, int x2, int y2, int color) {
        int dx, dy;
        float k, g, h; // G= prirustek X, H = prirustek Y;
        dx = x2 - x1;
        dy = y2 - y1;
        k = dy / (float) dx;
        if (Math.abs(dx) > Math.abs(dy)) {
            g = 1; //jdeme po x - prirustek po 1
            h = k;
            if (x2 < x1) { // prohozeni
                int temp = x1;
                x1 = x2;
                x2 = temp;
                temp = y1;
                y1 = y2;
                y2 = temp;
            }
        } else {
            g = 1 / k;
            h = 1;//jdeme po y - prirustek po 1
            if (y2 < y1) {// prohozeni
                int temp = x1;
                x1 = x2;
                x2 = temp;
                temp = y1;
                y1 = y2;
                y2 = temp;
            }
        }
        float x = x1;
        float y = y1;

        for (int l = 1; l <= Math.max(Math.abs(dx), Math.abs(dy)); l++) {

            raster.drawPixel(Math.round(x), Math.round(y), color);
            x = x + g;
            y = y + h;
        }

    }

    public void drawLine(int x1, int y1, int x2, int y2, int color) {
        drawLineDDA(x1, y1, x2, y2, color);
    }

    public void drawRegularPolygon(int x1, int y1, int x2, int y2, int peakCount, int color) {
        double x0 = x2 - x1;
        double y0 = y2 - y1;
        double circleradius = 2 * Math.PI;
        double step = circleradius / (double) peakCount;
        for (double i = 0; i < circleradius; i += step) {
            double x = x0 * Math.cos(step) + y0 * Math.sin(step);
            double y = y0 * Math.cos(step) - x0 * Math.sin(step);

            drawLine((int) x0 + x1, (int) y0 + y1, (int) x + x1, (int) y + y1, color);

            x0 = x;
            y0 = y;
        }
    }

    public void drawPolygon(List<Point> points, int color) {
        if (points.size() < 3) {
            return;
        }

        Point previous = null;
        for (Point point:points){
            if (previous == null) {
                previous = point;
                continue;
            }
            drawLine(
                    previous.getX(),
                    previous.getY(),
                    point.getX(),
                    point.getY(),
                    color
            );
            previous = point;
        }
        drawLine(
                previous.getX(),
                previous.getY(),
                points.get(0).getX(),
                points.get(0).getY(),
                color
        );
    }

    public List<Point> clip(List<Point> polygonPoints /*clipPoly*/, List<Point> clipPoints /* clipper */) {
        List<Point> out = new ArrayList<>();

        for (Point p : polygonPoints) {
            out.add(p);
        }


        for (int i = 0; i < clipPoints.size(); i++) {
            Edge cutter = new Edge(clipPoints.get(i), clipPoints.get((i + 1) % clipPoints.size()));

            polygonPoints.clear();
            for (Point b : out) {
                polygonPoints.add(b);
            }
            out.clear();

            Point v1 = polygonPoints.get(polygonPoints.size() - 1);
            for (int j = 0; j < polygonPoints.size(); j++) {
                Point v2 = polygonPoints.get(j);

                if (cutter.inside(v2)) {
                    if (!cutter.inside(v1)){
                        out.add(cutter.getIntersection(v1, v2));
                    }
                    out.add(v2);

                } else {
                    if (cutter.inside(v1)) {
                        out.add(cutter.getIntersection(v1, v2));
                    }

                }

                v1 = v2;
            }

        }
        return out;
    }
}