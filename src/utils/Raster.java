package utils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;

public class Raster extends JPanel {
    public static final int WIDTH = 800;
    public static final int HEIGHT = 640;

    private final int FPS = 1000 / 60;

    BufferedImage image;
    private Graphics g;

    public Raster() {
        image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        setSize(WIDTH, HEIGHT);
        setLoop();

        g = image.getGraphics();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null);
    }

    public void setLoop() {
        new Timer().schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        if (getGraphics() == null) {
                            return;
                        }
                        getGraphics().drawImage(image, 0, 0, null);
                    }
                },
                0,
                FPS
        );
    }

    public void drawPixel(int x, int y, int color) {
        if (x < 0 || x > getWidth() || y < 0 || y > getHeight()) {
            return;
        }
        image.setRGB(x, y, color);
    }

    public int getPixel(int x, int y) {
        return image.getRGB(x, y);
    }

    public void clear() {
        image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        g = image.getGraphics();
    }

    public BufferedImage getImg() {
        return image;
    }

    public void drawLine(double x1, double y1, double x2, double y2, Color color) {
        g.setColor(color);
        g.drawLine(
                (int) Math.round(x1),
                (int) Math.round(y1),
                (int) Math.round(x2),
                (int) Math.round(y2)
        );
    }
}
