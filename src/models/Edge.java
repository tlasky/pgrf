package models;

public class Edge {
    Point p1, p2;

    public Edge(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public boolean isHorizontal() {
        return p1.getY() == p2.getY();
    }

    public void orientate() {
        if (p1.getY() > p2.getY()) {
            Point temp = new Point(p1);
            p1 = new Point(p2);
            p2 = new Point(temp);
        }
    }

    public boolean intersectionExists(int y) {
        return (y >= p1.getY() && y < p2.getY());
    }

    public int getIntersection(int y) {
        float dx = (float) (p2.getX() - p1.getX());
        float dy = (float) (p2.getY() - p1.getY());
        float k = dx / dy;
        float q = (p1.getX() - (k * p1.getY()));
        float x = (k * y) + q;
        return (int) x;
    }

    public boolean inside(Point p) {
        Point t = new Point(
                p2.getX() - p1.getX(),
                p2.getY() - p1.getY()
        );
        Point n = new Point(-t.y, t.x);
        Point v = new Point(
                p.x - p1.getX(),
                p.y - p1.getY()
        );
        return (v.x * n.x + v.y * n.y < 0);
    }

    public Point getIntersection(Point v1, Point v2) {
        float x0 = ((v1.x * v2.y - v1.y * v2.x) * (p1.getX() - p2.getX()) - (p1.getX() * p2.getY() - p1.getY() * p2.getX()) * (v1.x - v2.x))
                / (float) ((v1.x - v2.x) * (p1.getY() - p2.getY()) - (p1.getX() - p2.getX()) * (v1.y - v2.y));
        float y0 = ((v1.x * v2.y - v1.y * v2.x) * (p1.getY() - p2.getY()) - (p1.getX() * p2.getY() - p1.getY() * p2.getX()) * (v1.y - v2.y))
                / (float) ((v1.x - v2.x) * (p1.getY() - p2.getY()) - (p1.getX() - p2.getX()) * (v1.y - v2.y));
        return new Point(Math.round(x0), Math.round(y0));
    }
}
