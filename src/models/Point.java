package models;

import java.awt.event.MouseEvent;

/*
* Point s Bulder settery.
* */

public class Point {
    public int x, y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point(Point p) {
        this.x = p.getX();
        this.y = p.getY();
    }

    public Point(MouseEvent e) {
        this.x = e.getX();
        this.y = e.getY();
    }

    public int getX() {
        return x;
    }

    public Point setX(int x) {
        this.x = x;
        return this;
    }

    public int getY() {
        return y;
    }

    public Point setY(int y) {
        this.y = y;
        return this;
    }

    @Override
    public String toString() {
        return String.format(
                "Point(%d\t%d)",
                x,
                y
        );
    }
}
