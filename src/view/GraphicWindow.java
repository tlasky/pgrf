package view;

import utils.Raster;
import utils.Renderer;

import javax.swing.*;


public class GraphicWindow extends JFrame {
    Renderer renderer;
    Raster raster;

    public GraphicWindow() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle("GraphicsFrame");
        setSize(raster.WIDTH, raster.HEIGHT);
        setResizable(false);

        raster = new Raster();
        raster.setFocusable(true);
        raster.grabFocus();
        add(raster);
    }

    public Raster getRaster() {
        return raster;
    }
}
