package ThreeD.app;

import ThreeD.controller.Controller3D;
import view.GraphicWindow;

import javax.swing.*;

/*
 * https://github.com/milankostak/PGRF1-2018
 * */

public class App3D {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            GraphicWindow window = new GraphicWindow();
            //new Controller(window);
            new Controller3D(window.getRaster());
            window.setVisible(true);
        });
        // https://www.google.com/search?q=SwingUtilities.invokeLater
        // https://www.javamex.com/tutorials/threads/invokelater.shtml
    }
}
