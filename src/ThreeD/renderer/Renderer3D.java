package ThreeD.renderer;

import ThreeD.models.Solid;
import transforms.*;
import utils.Raster;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Renderer3D {
    private Raster raster;
    private Mat4 model;
    private Mat4 view;
    private Mat4 projection;

    private List<Solid> solids = new ArrayList<>();

    public Renderer3D(Raster raster) {
        this.raster = raster;

        model = new Mat4Identity();

        Vec3D e = new Vec3D(0, -5, 4);
        Vec3D v = new Vec3D(0, 5, -4);
        Vec3D u = new Vec3D(0, 0, 1);

        view = new Mat4ViewRH(e, v, u);

        projection = new Mat4PerspRH(
                Math.PI / 4,
                raster.HEIGHT / (double) Raster.WIDTH,
                0.1,
                200
        );
    }

    public void add(Solid ... solids) {
        this.solids.addAll(Arrays.asList(solids));
        repaint();
    }

    public void repaint() {
        raster.clear();
        for (Solid solid : solids) {
            List<Point3D> vertices = solid.getVertices();
            List<Integer> indices = solid.getIndices();
            for (int i = 0; i < solid.getIndices().size(); i += 2) {
                Integer i1 = indices.get(i);
                Point3D a = vertices.get(i1);
                Integer i2 = indices.get(i + 1);
                Point3D b = vertices.get(i2);
                drawLine(a, b, solid.getColor());
            }
        }
    }

    private void drawLine(Point3D a, Point3D b, Color color) {
        a = a.mul(model).mul(view).mul(projection);
        b = b.mul(model).mul(view).mul(projection);

        if (!a.dehomog().isPresent()  || !b.dehomog().isPresent()) {
            return;
        }

        Vec3D v1 = a.dehomog().get();
        Vec3D v2 = b.dehomog().get();

        if (Math.abs(v1.getX()) > 1 || Math.abs(v1.getY()) > 1 || v1.getZ() < 0 || v1.getZ() > 1) return;
        if (Math.abs(v2.getX()) > 1 || Math.abs(v2.getY()) > 1 || v2.getZ() < 0 || v2.getZ() > 1) return;

        v1 = transformToWindow(v1);
        v2 = transformToWindow(v2);

        raster.drawLine(v1.getX(), v1.getY(), v2.getX(), v2.getY(), color);
    }

    private Vec3D transformToWindow(Vec3D v) {
        return v
                .mul(new Vec3D(1, -1, 1))
                .add(new Vec3D(1, 1, 0))
                .mul(
                    new Vec3D(
                    Raster.WIDTH / 2f,
                    Raster.HEIGHT / 2f,
                          1
                    )
                );
    }

    public Mat4 getModel() {
        return model;
    }

    public Renderer3D setModel(Mat4 model) {
        this.model = model;
        repaint();
        return this;
    }

    public Mat4 getView() {
        return view;
    }

    public Renderer3D setView(Mat4 view) {
        this.view = view;
        repaint();
        return this;
    }

    public Mat4 getProjection() {
        return projection;
    }

    public Renderer3D setProjection(Mat4 projection) {
        this.projection = projection;
        return this;
    }
}
