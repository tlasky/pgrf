package ThreeD.models;

import transforms.Point3D;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Solid {
    Color color;
    List<Point3D> vertices = new ArrayList<>();
    List<Integer> indices = new ArrayList<>();

    public Color getColor() {
        return color;
    }

    public List<Point3D> getVertices() {
        return vertices;
    }

    public List<Integer> getIndices() {
        return indices;
    }
}
