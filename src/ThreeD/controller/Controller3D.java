package ThreeD.controller;


import ThreeD.models.Cubic3D;
import ThreeD.models.Solid;
import ThreeD.models.Spiral;
import ThreeD.renderer.Renderer3D;
import transforms.*;
import utils.Raster;

import javax.swing.*;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Controller3D {
    private final Renderer3D renderer3D;

    private Camera camera;
    private Point2D mousePosition;

    private Solid solid;

    public Controller3D(Raster raster) {
        renderer3D = new Renderer3D(raster);

        initObjects();
        initListeners(raster);
        draw();
    }

    private void resetCamera() {
        camera = new Camera(
                new Vec3D(0, -5, 4),
                Math.toRadians(90), // <-pi/2, pi/2>
                Math.toRadians(-40),
                1,
                true
        );
        renderer3D.setView(camera.getViewMatrix());
    }

    public void initObjects() {
        solid = new Spiral();
        resetCamera();
    }

    public void initListeners(Raster raster) {
        raster.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                mousePosition = new Point2D(e.getX(), e.getY());

            }
        });

        raster.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    double diffx = (mousePosition.getX() - e.getX()) / -200.;
                    double diffy = (mousePosition.getY() - e.getX()) / -2000.;
                    double azimuth = camera.getAzimuth() + diffx;
                    double zenith = camera.getZenith() + diffy;
                    camera = camera.withAzimuth(azimuth);
                    //camera = camera.withZenith(zenith);
                    renderer3D.setView(camera.getViewMatrix());
                } else {
                    double rotX = (mousePosition.getX() - e.getX()) / -200.;
                    double rotY = (mousePosition.getY() - e.getY()) / - 200.;
                    Mat4 rot = renderer3D.getModel().mul(new Mat4RotXYZ(rotY, 0, rotX));
                    renderer3D.setModel(rot);
                }
                mousePosition = new Point2D(e.getX(), e.getY());
            }
        });

        raster.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_W:
                    case KeyEvent.VK_UP:
                        camera = camera.forward(1);
                        renderer3D.setView(camera.getViewMatrix());
                        break;
                    case KeyEvent.VK_S:
                    case KeyEvent.VK_DOWN:
                        camera = camera.backward(1);
                        renderer3D.setView(camera.getViewMatrix());
                        break;
                    case KeyEvent.VK_A:
                    case KeyEvent.VK_LEFT:
                        camera = camera.left(1);
                        renderer3D.setView(camera.getViewMatrix());
                        break;
                    case KeyEvent.VK_D:
                    case KeyEvent.VK_RIGHT:
                        camera = camera.right(1);
                        renderer3D.setView(camera.getViewMatrix());
                        break;
                }
            }
        });
    }

    private void draw() {
        renderer3D.add(solid);
    }
}
