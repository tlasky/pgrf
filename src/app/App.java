package app;

import controller.GraphicController;
import view.GraphicWindow;

import javax.swing.*;

/*
* https://github.com/milankostak/PGRF1-2018
* */

public class App {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            GraphicWindow window = new GraphicWindow();
            new GraphicController(window);
            window.setVisible(true);
        });
    }
}
